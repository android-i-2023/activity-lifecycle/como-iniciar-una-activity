package com.utn.ejemplo.startactivity2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class Activity2 : AppCompatActivity() {
    companion object {
        const val DIA = "diaDeLaSemana"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_2)

        // !! obliga a que esto exista, o se lanza excepcion
        val dia = intent.getStringExtra(DIA)!!
        val texto: TextView = findViewById(R.id.texto)
        texto.text = getString(R.string.hoy_es___baby, dia)
    }
}