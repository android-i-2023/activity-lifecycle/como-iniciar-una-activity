package com.utn.ejemplo.startactivity2

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import androidx.activity.result.ActivityResultCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContract
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var lanzadorCambiarFondo: ActivityResultLauncher<String>

    private lateinit var fondo: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /* Este objeto tiene dos responsabilidades:
            1. Define cómo se invoca a la Activity que queremos lanzar
               (es decir, cómo se prepara el Intent)
            2. Define cómo se interpreta el resultado que nos devuelve la Activity que lanzamos
               (es decir, cómo se lee el Intent que esa Activity nos devuelve)
         */
        val contrato = object : ActivityResultContract<String, Uri?>() {
            override fun createIntent(context: Context, input: String): Intent {
                val intent = Intent(Intent.ACTION_PICK)
                intent.type = input
                // No lanzamos el Intent, sólo lo retornamos.
                return intent
            }

            override fun parseResult(resultCode: Int, intent: Intent?): Uri? {
                // Sólo nos interesa analizar RESULT_OK.
                // Si el resultado fuera distinto, la Galería no nos hubiera pasado la Uri de una imagen
                if (resultCode == RESULT_OK) {
                    // El if de abajo es equivalente a:
                    // return intent?.data
                    if (intent == null) {
                        return null
                    } else {
                        return intent.data
                    }
                } else {
                    return null
                }
            }
        }

        // ActivityResultCallback sólo tiene una responsabilidad: indica qué hacer
        // con el resultado que nos devolvió la Activity que lanzamos
        // En nuestro caso asignamos la Uri de la foto de Galería seleccionada
        val callback = object : ActivityResultCallback<Uri?> {
            override fun onActivityResult(result: Uri?) {
                Log.d("CURSO", "Recibimos la URI de una foto: $result")
                fondo.setImageURI(result)
            }
        }

        lanzadorCambiarFondo = registerForActivityResult(contrato, callback)
        fondo = findViewById(R.id.fondo)

        val lanzarActivity = findViewById<Button>(R.id.btn_lanzar_activity_vacia)
        val marcarTelefono = findViewById<Button>(R.id.btn_marcar)
        val enviarMensaje = findViewById<Button>(R.id.btn_enviar_mensaje)
        val cambiarFondo = findViewById<Button>(R.id.btn_cambiar_fondo)

        lanzarActivity.setOnClickListener(this)
        marcarTelefono.setOnClickListener(this)
        enviarMensaje.setOnClickListener(this)
        cambiarFondo.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.btn_lanzar_activity_vacia -> lanzarActivityVacia()
            R.id.btn_marcar -> marcarNumero("11-2233-4455")
            R.id.btn_enviar_mensaje -> compartirMensaje("Mi super mensaje")
            R.id.btn_cambiar_fondo -> cambiarFondo()
        }
    }

    private fun lanzarActivityVacia() {
        val intent = Intent(this, Activity2::class.java)
        intent.putExtra(Activity2.DIA, "martes")
        startActivity(intent)
    }

    private fun marcarNumero(nroTelefono: String) {
        val intent = Intent(Intent.ACTION_DIAL)

        // Uri -> Universal Resource Identifier
        // "tel" es el _esquema_ de la Uri
        intent.data = Uri.parse("tel:$nroTelefono")

        startActivity(intent)
    }

    private fun compartirMensaje(mensaje: String) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_TEXT, mensaje)

        intent.type = "text/plain"
        intent.setPackage("com.whatsapp")

        // Cómo decidir en qué versión de Android se está ejecutando esta Activity?
        // Será superior a Android 6?
        val banderas = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            PackageManager.MATCH_ALL
        } else {
            0
        }

        val resolveInfos = packageManager.queryIntentActivities(intent, banderas)
        if (resolveInfos.isEmpty()) {
            Log.e("CURSO", "No hay como abrir este intent: $intent")
        } else {
            startActivity(intent)
        }
    }

    private fun cambiarFondo() {
        lanzadorCambiarFondo.launch("image/*")
    }
}
